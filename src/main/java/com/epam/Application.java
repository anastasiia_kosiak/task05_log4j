package com.epam;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.IOException;
public class Application {
    private static Logger logger = LogManager.getLogger(Application.class);
    public static void main(String[] args) {
        logger.error("Exception", new IOException("Testing a logger"));
        logger.trace("Logger trace message");
        logger.debug("Logger debug message");
        logger.info("Logger info message");
        logger.warn("Logger warn message");
        logger.error("Logger error message");
        logger.fatal("Logger fatal message");
    }
}
